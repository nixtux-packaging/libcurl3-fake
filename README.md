# Fake libcurl3 package for Debian/Ubuntu

Deb-пакеты некоторых кривых проприетарных поделий, например, BricsCAD 18.1, зависят от пакета `libcurl3`, хотя на самом деле они носят библиотеку `libcurl.so`, и им не нужна эта зависимость!

`libcurl3` убрали из новых выпусков Debian и Ubuntu, т.к. это старая версия библиотеки с незакрытыми уязвимостями. Актуален `libcurl4`.

Этот пакет просто удовлетворяет чьи-то зависимости от `libcurl3` и больше ничего не делает.

------------------
Some buggy proprietary deb packages, e.g. BricsCAD 18.1, depend from package `libcurl3`, but they carry `libcurl.so` with them, and do not need this dependency!

libcurl3 was dropped from Debian because it's an old version with many vulnarabilities! `libcurl4` is present.

This package simply satisfies the dependency `libcurl3` and does nothing else.

------------------
To build & install this package (чтобы собрать и установить этот пакет):

```
sudo apt install devscripts
git clone https://gitlab.com/nixtux-packaging/libcurl3-fake.git
cd libcurl3-fake
dpkg-buildpackage
debian/rules clean
sudo apt install ../libcurl3-fake_*.deb
```